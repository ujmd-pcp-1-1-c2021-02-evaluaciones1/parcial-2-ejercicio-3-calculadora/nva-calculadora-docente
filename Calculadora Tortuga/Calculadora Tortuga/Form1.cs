﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora_Tortuga
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            double Operando1 = Convert.ToDouble(txtOperando1.Text);
            double Operando2 = Convert.ToDouble(txtOperando2.Text);

            double Resultado = 0;

            if (rbtnSuma.Checked)
            {
                Resultado = Operando1 + Operando2;
            }
            else if (rbtnResta.Checked)
            {
                Resultado = Operando1 - Operando2;
            }
            else if (rbtnMultiplicación.Checked)
            {
                Resultado = Operando1 * Operando2;
            }
            else if (rbtnDivisión.Checked)
            {
                Resultado = Operando1 / Operando2;
            }

            txtResultado.Text = Resultado.ToString();
        }
    }
}
