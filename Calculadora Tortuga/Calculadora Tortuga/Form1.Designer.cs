﻿
namespace Calculadora_Tortuga
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtOperando1 = new System.Windows.Forms.TextBox();
            this.txtOperando2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtnSuma = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.rbtnResta = new System.Windows.Forms.RadioButton();
            this.rbtnMultiplicación = new System.Windows.Forms.RadioButton();
            this.rbtnDivisión = new System.Windows.Forms.RadioButton();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.txtResultado, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtOperando1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtOperando2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnCalcular, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(267, 219);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // txtOperando1
            // 
            this.txtOperando1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOperando1.Location = new System.Drawing.Point(3, 25);
            this.txtOperando1.Multiline = true;
            this.txtOperando1.Name = "txtOperando1";
            this.txtOperando1.Size = new System.Drawing.Size(127, 29);
            this.txtOperando1.TabIndex = 0;
            this.txtOperando1.Text = "0";
            this.txtOperando1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtOperando2
            // 
            this.txtOperando2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOperando2.Location = new System.Drawing.Point(136, 25);
            this.txtOperando2.Multiline = true;
            this.txtOperando2.Name = "txtOperando2";
            this.txtOperando2.Size = new System.Drawing.Size(128, 29);
            this.txtOperando2.TabIndex = 0;
            this.txtOperando2.Text = "0";
            this.txtOperando2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "Operando 1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label2.Location = new System.Drawing.Point(136, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "Operando 2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // rbtnSuma
            // 
            this.rbtnSuma.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnSuma.Checked = true;
            this.rbtnSuma.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnSuma.Location = new System.Drawing.Point(93, 3);
            this.rbtnSuma.Name = "rbtnSuma";
            this.rbtnSuma.Size = new System.Drawing.Size(34, 28);
            this.rbtnSuma.TabIndex = 2;
            this.rbtnSuma.TabStop = true;
            this.rbtnSuma.Text = "+";
            this.rbtnSuma.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnSuma.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.rbtnSuma, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.rbtnResta, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.rbtnMultiplicación, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.rbtnDivisión, 2, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 60);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel1.SetRowSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(261, 69);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // rbtnResta
            // 
            this.rbtnResta.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnResta.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnResta.Location = new System.Drawing.Point(133, 3);
            this.rbtnResta.Name = "rbtnResta";
            this.rbtnResta.Size = new System.Drawing.Size(34, 28);
            this.rbtnResta.TabIndex = 2;
            this.rbtnResta.Text = "-";
            this.rbtnResta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnResta.UseVisualStyleBackColor = true;
            // 
            // rbtnMultiplicación
            // 
            this.rbtnMultiplicación.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnMultiplicación.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnMultiplicación.Location = new System.Drawing.Point(93, 37);
            this.rbtnMultiplicación.Name = "rbtnMultiplicación";
            this.rbtnMultiplicación.Size = new System.Drawing.Size(34, 28);
            this.rbtnMultiplicación.TabIndex = 2;
            this.rbtnMultiplicación.Text = "*";
            this.rbtnMultiplicación.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnMultiplicación.UseVisualStyleBackColor = true;
            // 
            // rbtnDivisión
            // 
            this.rbtnDivisión.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnDivisión.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnDivisión.Location = new System.Drawing.Point(133, 37);
            this.rbtnDivisión.Name = "rbtnDivisión";
            this.rbtnDivisión.Size = new System.Drawing.Size(34, 28);
            this.rbtnDivisión.TabIndex = 2;
            this.rbtnDivisión.Text = "/";
            this.rbtnDivisión.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnDivisión.UseVisualStyleBackColor = true;
            // 
            // btnCalcular
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.btnCalcular, 2);
            this.btnCalcular.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCalcular.Location = new System.Drawing.Point(40, 135);
            this.btnCalcular.Margin = new System.Windows.Forms.Padding(40, 3, 40, 3);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(187, 26);
            this.btnCalcular.TabIndex = 4;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // txtResultado
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtResultado, 2);
            this.txtResultado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResultado.Location = new System.Drawing.Point(60, 167);
            this.txtResultado.Margin = new System.Windows.Forms.Padding(60, 3, 60, 3);
            this.txtResultado.Multiline = true;
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.ReadOnly = true;
            this.txtResultado.Size = new System.Drawing.Size(147, 34);
            this.txtResultado.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.ClientSize = new System.Drawing.Size(267, 219);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Calculadora";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox txtResultado;
        private System.Windows.Forms.TextBox txtOperando1;
        private System.Windows.Forms.TextBox txtOperando2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.RadioButton rbtnSuma;
        private System.Windows.Forms.RadioButton rbtnResta;
        private System.Windows.Forms.RadioButton rbtnMultiplicación;
        private System.Windows.Forms.RadioButton rbtnDivisión;
        private System.Windows.Forms.Button btnCalcular;
    }
}

